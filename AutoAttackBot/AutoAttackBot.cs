﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Core.Movement;
using AOSharp.Common.GameData;
using System.IO;
using AOSharp.Core.GameData;
using AOSharp.Core.UI.Options;
using AOSharp.Pathfinding;
using System.Data;
using AOSharp.Core.IPC;
using AutoAttackBot.IPCMessages;
using AOSharp.Core.Inventory;
using System.Collections.Concurrent;

namespace AutoAttackBot
{
    public class AutoAttackBot : AOPluginEntry
    {
        public static StateMachine _stateMachine;
        public static NavMeshMovementController NavMeshMovementController { get; private set; }
        public static IPCChannel IPCChannel { get; private set; }

        private Settings settings = new Settings("AutoAttackBot");

        public static List<string> _namesToIgnore;

        public static bool Running = false;
        public static bool RunningDefend = false;
        public static Identity Leader = Identity.None;
        public static bool IsLeader = false;

        public override void Run(string pluginDir)
        {
            Chat.WriteLine("AutoAttackBot Loaded!");

            try
            {
                _stateMachine = new StateMachine(new IdleState());

                Chat.RegisterCommand("auto", AutoAttackBotCommand);

                _namesToIgnore = new List<string>
                {
                    "Spirit of Judgement",
                    "Wandering Spirit",
                    "Altar of Torture",
                    "Altar of Purification",
                    "Unicorn Coordinator Magnum Blaine",
                    "Xan Spirit",
                    "Watchful Spirit",
                    "Amesha Vizaresh",
                    "Guardian Spirit of Purification",
                    "Tibor 'Rocketman' Nagy",
                    "One Who Obeys Precepts",
                    "The Retainer Of Ergo",
                    "Outzone Supplier",
                    "Hollow Island Weed",
                    "Sheila Marlene",
                    "Unicorn Advance Sentry",
                    "Unicorn Technician",
                    "Basic Tools Merchant",
                    "Container Supplier",
                    "Basic Quality Pharmacist",
                    "Basic Quality Armorer",
                    "Basic Quality Weaponsdealer",
                    "Tailor",
                    "Unicorn Commander Rufus",
                    "Ergo, Inferno Guardian of Shadows",
                    "Unicorn Trooper",
                    "Unicorn Squadleader",
                    "Rookie Alien Hunter",
                    "Unicorn Service Tower Alpha",
                    "Unicorn Service Tower Delta",
                    "Unicorn Service Tower Gamma",
                    "Sean Powell",
                    "Xan Spirit",
                    "Unicorn Guard",
                    "Essence Fragment",
                    "Awakened Xan"
                };

                IPCChannel = new IPCChannel(12);
                IPCChannel.RegisterCallback((int)IPCOpcode.StartDefend, OnStartMessageDefend);
                IPCChannel.RegisterCallback((int)IPCOpcode.StopDefend, OnStopMessageDefend);
                IPCChannel.RegisterCallback((int)IPCOpcode.StartAuto, OnStartMessageAuto);
                IPCChannel.RegisterCallback((int)IPCOpcode.StopAuto, OnStopMessageAuto);
                IPCChannel.RegisterCallback((int)IPCOpcode.SetPos, OnSetPosMessage);

                settings.AddVariable("Follow", false);

                Game.OnUpdate += OnUpdate;
            }
            catch(Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }

        private void StartAuto()
        {
            Chat.WriteLine("OnStartMessage");

            Running = true;

            if(!IsLeader && !(_stateMachine.CurrentState is IdleState))
                _stateMachine.SetState(new IdleState());
        }

        private void StopAuto()
        {
            Chat.WriteLine("OnStopMessage");

            Running = false;

            _stateMachine.SetState(new IdleState());
        }





        private void StartDefend()
        {
            Chat.WriteLine("OnStartMessage");

            RunningDefend = true;

            if (!IsLeader && !(_stateMachine.CurrentState is IdleState))
                _stateMachine.SetState(new IdleState());
        }

        private void StopDefend()
        {
            RunningDefend = false;

            Chat.WriteLine("OnStopMessage");

            _stateMachine.SetState(new IdleState());
        }




        private void OnStartMessageAuto(int sender, IPCMessage msg)
        {
            StartMessageAuto startMsgAuto = (StartMessageAuto)msg;

            Leader = new Identity(IdentityType.SimpleChar, sender);

            StartAuto();
        }

        private void OnStopMessageAuto(int sender, IPCMessage msg)
        {
            StopMessageAuto stopMsgAuto = (StopMessageAuto)msg;

            StopAuto();
        }



        private void OnStartMessageDefend(int sender, IPCMessage msg)
        {
            StartMessageDefend startMsgDefend = (StartMessageDefend)msg;

            Leader = new Identity(IdentityType.SimpleChar, sender);

            StartDefend();
        }

        private void OnStopMessageDefend(int sender, IPCMessage msg)
        {
            StopMessageDefend stopMsgDefend = (StopMessageDefend)msg;

            StopDefend();
        }







        private void SetPos()
        {
            Chat.WriteLine("Position Set.");

            Constants._posToDefend = DynelManager.LocalPlayer.Position;
        }

        private void OnSetPosMessage(int sender, IPCMessage msg)
        {
            SetPosMessage setposMsg = (SetPosMessage)msg;

            SetPos();
        }






        private void OnUpdate(object s, float deltaTime)
        {
            if (Game.IsZoning)
                return;

            _stateMachine.Tick();

            if (!IsLeader)
                return;
        }

        private void PrintAutoAttackBotCommandUsage(ChatWindow chatWindow)
        {
            string help = "For defending a position with leash;\n" +
                            "\n" +
                            "/auto setpos then /auto defend to turn on and off.\n" +
                            "\n" +
                            "\n" +
                            "For attacking anything in range;\n" +
                            "\n" +
                            "/auto attack to turn on and off.";

            chatWindow.WriteLine(help, ChatColor.LightBlue);
        }

        private void AutoAttackBotCommand(string command, string[] param, ChatWindow chatWindow)
        {
            try
            {
                if (param.Length < 1)
                {
                    PrintAutoAttackBotCommandUsage(chatWindow);
                    return;
                }

                if (param[0].ToLower() == "help" || param.Length < 1)
                {
                    PrintAutoAttackBotCommandUsage(chatWindow);
                    return;
                }

                if (param[0].ToLower() == "setpos")
                {
                    SetPos();
                    IPCChannel.Broadcast(new SetPosMessage());
                }

                if (param[0].ToLower() == "attack")
                {
                    if (RunningDefend)
                    {
                        Chat.WriteLine("You have to disable /auto defend to use /auto.");
                        return;
                    }

                    if (Running)
                    {
                        StopAuto();
                        IPCChannel.Broadcast(new StopMessageAuto());
                    }
                    else
                    {
                        StartAuto();
                        IPCChannel.Broadcast(new StartMessageAuto());
                    }
                }

                if (param[0].ToLower() == "defend")
                {
                    if (Running)
                    {
                        Chat.WriteLine("You have to disable /auto to use /auto defend.");
                        return;
                    }

                    if (Constants._posToDefend != Vector3.Zero)
                    {
                        if (RunningDefend)
                        {
                            StopDefend();

                            IPCChannel.Broadcast(new StopMessageDefend());
                        }
                        else
                        {
                            if (Constants._posToDefend != Vector3.Zero && DynelManager.LocalPlayer.Position.DistanceFrom(Constants._posToDefend) > 15f)
                            {
                                Chat.WriteLine("You are not near your defence position.");
                            }
                            else
                            {
                                StartDefend();

                                IPCChannel.Broadcast(new StartMessageDefend());
                            }
                        }
                    }
                    else
                    {
                        Chat.WriteLine("You need to /auto setpos before starting.");
                    }
                }

            } 
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }
    }
}
