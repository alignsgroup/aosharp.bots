﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoAttackBot.IPCMessages
{
    public enum IPCOpcode
    {
        StartDefend = 1001,
        StopDefend = 1002,
        StartAuto = 1003,
        StopAuto = 1005,
        SetPos = 1006
    }
}
