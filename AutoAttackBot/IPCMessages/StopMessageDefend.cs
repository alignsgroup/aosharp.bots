﻿using System;
using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;

namespace AutoAttackBot.IPCMessages
{
    [AoContract((int)IPCOpcode.StopDefend)]
    public class StopMessageDefend : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.StopDefend;
    }
}
