﻿using System;
using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;

namespace AutoAttackBot.IPCMessages
{
    [AoContract((int)IPCOpcode.StopAuto)]
    public class StopMessageAuto : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.StopAuto;
    }
}
