﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace AutoAttackBot
{
    public class FightState : PositionHolder, IState
    {
        public const double FightTimeout = 60f;
        private SimpleChar _target;
        private double _fightStartTime;
        public static float _tetherDistance;
        public SimpleChar LeaderChar;

        public FightState(SimpleChar target) : base(Constants._posToDefend, 3f, 1)
        {
            _target = target;
        }

        public IState GetNextState()
        {
            if (AutoAttackBot.RunningDefend)
            {
                if (!_target.IsValid ||
                   !_target.IsAlive ||
                   _target.DistanceFrom(DynelManager.LocalPlayer) > 37f ||
                   !_target.IsInLineOfSight ||
                   DynelManager.LocalPlayer.Buffs.Contains(280470) ||
                   (Time.NormalTime > _fightStartTime + FightTimeout && _target.MaxHealth <= 999999))
                {
                    _target = null;
                    return new DefendState();
                }
            }
            else
            {
                if (!_target.IsValid ||
                   !_target.IsAlive ||
                   _target.DistanceFrom(DynelManager.LocalPlayer) > 37f ||
                   !_target.IsInLineOfSight ||
                   DynelManager.LocalPlayer.Buffs.Contains(280470) ||
                   (Time.NormalTime > _fightStartTime + FightTimeout && _target.MaxHealth <= 999999))
                {
                    _target = null;
                    return new ScanState(); // scan state?
                }
            }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("FightState::OnStateEnter");
            _fightStartTime = Time.NormalTime;
        }

        public void OnStateExit()
        {
            Chat.WriteLine("FightState::OnStateExit");

            if (DynelManager.LocalPlayer.IsAttacking)
                DynelManager.LocalPlayer.StopAttack();
        }

        private bool IsNotFightingMe(SimpleChar target)
        {
            return target.IsAttacking && target.FightingTarget.Identity != DynelManager.LocalPlayer.Identity;
        }

        private bool IsNotFightingAnyTeamMember(SimpleChar target)
        {
            if (Team.IsInTeam)
            {
                // maybe some sort of assist function??
                return (target.IsAttacking && Team.Members.Any(x => target.FightingTarget.Identity == x.Character.Identity)) ||
                    (target.IsAttacking && Team.Members.Where(x => x.Character.FightingTarget != null).Any(x => x.Character.FightingTarget.Identity == target.Identity));
            }
            else
            {
                return target.IsAttacking && target.FightingTarget.Identity == DynelManager.LocalPlayer.Identity ||
                    target.IsAttacking && DynelManager.LocalPlayer.Pets.Any(pet => target.FightingTarget.Identity == pet.Identity);
            }
        }

        public void Tick()
        {
            if (Team.IsInTeam)
            {
                TeamMember leader = Team.Members
                    .Where(x => x != null)
                    .Where(x => x.Character != null)
                    .Where(x => x.IsLeader)
                    .Where(x => x.Character.DistanceFrom(DynelManager.LocalPlayer) < 30f)
                    .FirstOrDefault();

                if (leader != null)
                {
                    List<SimpleChar> mobs = DynelManager.NPCs
                        .Where(x => x.Name == "Corrupted Xan-Len" || x.Name == "Hacker'Uri" || x.Name == "The Sacrifice" ||
                                x.Name == "Blue Tower" || x.Name == "Green Tower" || x.Name == "Alien Coccoon")
                        .Where(x => leader != null)
                        .Where(x => x.DistanceFrom(leader.Character) <= 30f)
                        .Where(x => !AutoAttackBot._namesToIgnore.Contains(x.Name))
                        .Where(x => x.IsAlive)
                        .Where(x => x.IsInLineOfSight)
                        .OrderBy(x => x.HealthPercent)
                        .ThenBy(x => x.DistanceFrom(leader.Character))
                        .ToList();

                    List<SimpleChar> all = DynelManager.NPCs
                        .Where(x => x.Name != "Corrupted Xan-Len" || x.Name != "Hacker'Uri" || x.Name != "The Sacrifice" ||
                                x.Name != "Blue Tower" || x.Name != "Green Tower" || x.Name != "Alien Coccoon")
                        .Where(x => leader != null)
                        .Where(x => x.DistanceFrom(leader.Character) <= 30f)
                        .Where(x => !AutoAttackBot._namesToIgnore.Contains(x.Name))
                        .Where(x => x.MaxHealth <= 999999)
                        .Where(x => x.IsAlive)
                        .Where(x => x.IsInLineOfSight)
                        .OrderBy(x => x.HealthPercent)
                        .ThenBy(x => x.DistanceFrom(leader.Character))
                        .ToList();

                    List<SimpleChar> boss = DynelManager.NPCs
                        .Where(x => leader != null)
                        .Where(x => x.DistanceFrom(leader.Character) <= 30f)
                        .Where(x => x.MaxHealth >= 1000000)
                        .Where(x => !AutoAttackBot._namesToIgnore.Contains(x.Name))
                        .Where(x => x.IsAlive)
                        .Where(x => x.IsInLineOfSight)
                        .ToList();

                    if (!_target.IsValid)
                        return;

                    if (AutoAttackBot.Running == true && !DynelManager.LocalPlayer.Buffs.Contains(280470))
                    {
                        if (mobs.Count >= 1 && !mobs.Contains(_target))
                        {
                            _target = mobs.FirstOrDefault();
                        }
                        if (all.Count >= 1 && !all.Contains(_target) && mobs.Count == 0)
                        {
                            if (DynelManager.LocalPlayer.FightingTarget != null && !all.Contains(DynelManager.LocalPlayer.FightingTarget))
                                _target = all.FirstOrDefault();
                        }
                        if (boss.Count >= 1 && !boss.Contains(_target) && mobs.Count == 0 && all.Count == 0)
                        {
                            _target = boss.FirstOrDefault();
                        }

                        if (!DynelManager.LocalPlayer.IsAttackPending && !DynelManager.LocalPlayer.IsAttacking)
                        {
                            DynelManager.LocalPlayer.Attack(_target);
                            _fightStartTime = Time.NormalTime;
                        }

                        if (DynelManager.LocalPlayer.FightingTarget != null && _target.Identity != DynelManager.LocalPlayer.FightingTarget.Identity)
                        {
                            DynelManager.LocalPlayer.Attack(_target);
                            _fightStartTime = Time.NormalTime;
                        }
                    }
                    else if (AutoAttackBot.Running == false && !DynelManager.LocalPlayer.Buffs.Contains(280470))
                    {
                        if (mobs.Count >= 1 && !mobs.Contains(_target))
                        {
                            _target = mobs.FirstOrDefault();
                        }
                        if (all.Count >= 1 && !all.Contains(_target) && mobs.Count == 0)
                        {
                            if (DynelManager.LocalPlayer.FightingTarget != null && !all.Contains(DynelManager.LocalPlayer.FightingTarget))
                                _target = all.FirstOrDefault();
                        }
                        if (boss.Count >= 1 && !boss.Contains(_target) && mobs.Count == 0 && all.Count == 0)
                        {
                            _target = boss.FirstOrDefault();
                        }

                        if (!_target.IsInAttackRange())
                        {
                            MovementController.Instance.SetDestination(_target.Position);
                        }

                        if (!DynelManager.LocalPlayer.IsAttackPending && !DynelManager.LocalPlayer.IsAttacking)
                        {
                            DynelManager.LocalPlayer.Attack(_target);
                            _fightStartTime = Time.NormalTime;
                        }

                        if (DynelManager.LocalPlayer.FightingTarget != null && _target.Identity != DynelManager.LocalPlayer.FightingTarget.Identity)
                        {
                            DynelManager.LocalPlayer.Attack(_target);
                            _fightStartTime = Time.NormalTime;
                        }
                    }
                }
                else
                {
                    List<SimpleChar> mobsnoteam = DynelManager.NPCs
                        .Where(x => x.Name == "Corrupted Xan-Len" || x.Name == "Hacker'Uri" || x.Name == "The Sacrifice" ||
                                x.Name == "Blue Tower" || x.Name == "Green Tower" || x.Name == "Alien Coccoon")
                        .Where(x => x.DistanceFrom(DynelManager.LocalPlayer) <= 30f)
                        .Where(x => !AutoAttackBot._namesToIgnore.Contains(x.Name))
                        .Where(x => x.IsAlive)
                        .Where(x => x.IsInLineOfSight)
                        .OrderBy(x => x.HealthPercent)
                        .ThenBy(x => x.DistanceFrom(DynelManager.LocalPlayer))
                        .ToList();

                    List<SimpleChar> allnoteam = DynelManager.NPCs
                        .Where(x => x.Name != "Corrupted Xan-Len" || x.Name != "Hacker'Uri" || x.Name != "The Sacrifice" ||
                                x.Name != "Blue Tower" || x.Name != "Green Tower" || x.Name != "Alien Coccoon")
                        .Where(x => x.DistanceFrom(DynelManager.LocalPlayer) <= 30f)
                        .Where(x => !AutoAttackBot._namesToIgnore.Contains(x.Name))
                        .Where(x => x.MaxHealth <= 999999)
                        .Where(x => x.IsAlive)
                        .Where(x => x.IsInLineOfSight)
                        .OrderBy(x => x.HealthPercent)
                        .ThenBy(x => x.DistanceFrom(DynelManager.LocalPlayer))
                        .ToList();

                    List<SimpleChar> bossnoteam = DynelManager.NPCs
                        .Where(x => x.DistanceFrom(DynelManager.LocalPlayer) <= 30f)
                        .Where(x => x.MaxHealth >= 1000000)
                        .Where(x => !AutoAttackBot._namesToIgnore.Contains(x.Name))
                        .Where(x => x.IsAlive)
                        .Where(x => x.IsInLineOfSight)
                        .ToList();

                    if (!_target.IsValid)
                        return;

                    if (AutoAttackBot.Running == true)
                    {
                        if (mobsnoteam.Count >= 1 && !mobsnoteam.Contains(_target))
                        {
                            _target = mobsnoteam.FirstOrDefault();
                        }
                        if (allnoteam.Count >= 1 && !allnoteam.Contains(_target) && mobsnoteam.Count == 0)
                        {
                            if (DynelManager.LocalPlayer.FightingTarget != null && !allnoteam.Contains(DynelManager.LocalPlayer.FightingTarget))
                                _target = allnoteam.FirstOrDefault();
                        }
                        if (bossnoteam.Count >= 1 && !bossnoteam.Contains(_target) && mobsnoteam.Count == 0 && allnoteam.Count == 0)
                        {
                            _target = bossnoteam.FirstOrDefault();
                        }

                        if (!DynelManager.LocalPlayer.IsAttackPending && !DynelManager.LocalPlayer.IsAttacking)
                        {
                            DynelManager.LocalPlayer.Attack(_target);
                            _fightStartTime = Time.NormalTime;
                        }

                        if (DynelManager.LocalPlayer.FightingTarget != null && _target.Identity != DynelManager.LocalPlayer.FightingTarget.Identity)
                        {
                            DynelManager.LocalPlayer.Attack(_target);
                            _fightStartTime = Time.NormalTime;
                        }
                    }
                    else
                    {
                        if (mobsnoteam.Count >= 1 && !mobsnoteam.Contains(_target))
                        {
                            _target = mobsnoteam.FirstOrDefault();
                        }
                        if (allnoteam.Count >= 1 && !allnoteam.Contains(_target) && mobsnoteam.Count == 0)
                        {
                            if (DynelManager.LocalPlayer.FightingTarget != null && !allnoteam.Contains(DynelManager.LocalPlayer.FightingTarget))
                                _target = allnoteam.FirstOrDefault();
                        }
                        if (bossnoteam.Count >= 1 && !bossnoteam.Contains(_target) && mobsnoteam.Count == 0 && allnoteam.Count == 0)
                        {
                            _target = bossnoteam.FirstOrDefault();
                        }

                        if (!_target.IsInAttackRange())
                        {
                            MovementController.Instance.SetDestination(_target.Position);
                        }

                        if (!DynelManager.LocalPlayer.IsAttackPending && !DynelManager.LocalPlayer.IsAttacking)
                        {
                            DynelManager.LocalPlayer.Attack(_target);
                            _fightStartTime = Time.NormalTime;
                        }

                        if (DynelManager.LocalPlayer.FightingTarget != null && _target.Identity != DynelManager.LocalPlayer.FightingTarget.Identity)
                        {
                            DynelManager.LocalPlayer.Attack(_target);
                            _fightStartTime = Time.NormalTime;
                        }
                    }
                }

            }
            else
            {
                List<SimpleChar> mobsnoteam = DynelManager.NPCs
                    .Where(x => x.Name == "Corrupted Xan-Len" || x.Name == "Hacker'Uri" || x.Name == "The Sacrifice" ||
                            x.Name == "Blue Tower" || x.Name == "Green Tower" || x.Name == "Alien Coccoon")
                    .Where(x => x.DistanceFrom(DynelManager.LocalPlayer) <= 30f)
                    .Where(x => !AutoAttackBot._namesToIgnore.Contains(x.Name))
                    .Where(x => x.IsAlive)
                    .Where(x => x.IsInLineOfSight)
                    .OrderBy(x => x.HealthPercent)
                    .ThenBy(x => x.DistanceFrom(DynelManager.LocalPlayer))
                    .ToList();

                List<SimpleChar> allnoteam = DynelManager.NPCs
                    .Where(x => x.Name != "Corrupted Xan-Len" || x.Name != "Hacker'Uri" || x.Name != "The Sacrifice" ||
                            x.Name != "Blue Tower" || x.Name != "Green Tower" || x.Name != "Alien Coccoon")
                    .Where(x => x.DistanceFrom(DynelManager.LocalPlayer) <= 30f)
                    .Where(x => !AutoAttackBot._namesToIgnore.Contains(x.Name))
                    .Where(x => x.MaxHealth <= 999999)
                    .Where(x => x.IsAlive)
                    .Where(x => x.IsInLineOfSight)
                    .OrderBy(x => x.HealthPercent)
                    .ThenBy(x => x.DistanceFrom(DynelManager.LocalPlayer))
                    .ToList();

                List<SimpleChar> bossnoteam = DynelManager.NPCs
                    .Where(x => x.DistanceFrom(DynelManager.LocalPlayer) <= 30f)
                    .Where(x => x.MaxHealth >= 1000000)
                    .Where(x => !AutoAttackBot._namesToIgnore.Contains(x.Name))
                    .Where(x => x.IsAlive)
                    .Where(x => x.IsInLineOfSight)
                    .ToList();

                if (!_target.IsValid)
                    return;

                if (AutoAttackBot.Running == true)
                {
                    if (mobsnoteam.Count >= 1 && !mobsnoteam.Contains(_target))
                    {
                        _target = mobsnoteam.FirstOrDefault();
                    }
                    if (allnoteam.Count >= 1 && !allnoteam.Contains(_target) && mobsnoteam.Count == 0)
                    {
                        if (DynelManager.LocalPlayer.FightingTarget != null && !allnoteam.Contains(DynelManager.LocalPlayer.FightingTarget))
                            _target = allnoteam.FirstOrDefault();
                    }
                    if (bossnoteam.Count >= 1 && !bossnoteam.Contains(_target) && mobsnoteam.Count == 0 && allnoteam.Count == 0)
                    {
                        _target = bossnoteam.FirstOrDefault();
                    }

                    if (!DynelManager.LocalPlayer.IsAttackPending && !DynelManager.LocalPlayer.IsAttacking)
                    {
                        DynelManager.LocalPlayer.Attack(_target);
                        _fightStartTime = Time.NormalTime;
                    }

                    if (DynelManager.LocalPlayer.FightingTarget != null && _target.Identity != DynelManager.LocalPlayer.FightingTarget.Identity)
                    {
                        DynelManager.LocalPlayer.Attack(_target);
                        _fightStartTime = Time.NormalTime;
                    }
                }
                else
                {
                    if (mobsnoteam.Count >= 1 && !mobsnoteam.Contains(_target))
                    {
                        _target = mobsnoteam.FirstOrDefault();
                    }
                    if (allnoteam.Count >= 1 && !allnoteam.Contains(_target) && mobsnoteam.Count == 0)
                    {
                        if (DynelManager.LocalPlayer.FightingTarget != null && !allnoteam.Contains(DynelManager.LocalPlayer.FightingTarget))
                            _target = allnoteam.FirstOrDefault();
                    }
                    if (bossnoteam.Count >= 1 && !bossnoteam.Contains(_target) && mobsnoteam.Count == 0 && allnoteam.Count == 0)
                    {
                        _target = bossnoteam.FirstOrDefault();
                    }

                    if (!_target.IsInAttackRange())
                    {
                        MovementController.Instance.SetDestination(_target.Position);
                    }

                    if (!DynelManager.LocalPlayer.IsAttackPending && !DynelManager.LocalPlayer.IsAttacking)
                    {
                        DynelManager.LocalPlayer.Attack(_target);
                        _fightStartTime = Time.NormalTime;
                    }

                    if (DynelManager.LocalPlayer.FightingTarget != null && _target.Identity != DynelManager.LocalPlayer.FightingTarget.Identity)
                    {
                        DynelManager.LocalPlayer.Attack(_target);
                        _fightStartTime = Time.NormalTime;
                    }
                }
            }
        }
    }
}
