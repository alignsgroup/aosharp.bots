﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.UI;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AutoAttackBot
{
    public class ScanState : IState
    {
        private SimpleChar _target;

        public IState GetNextState()
        {
            if (_target != null)
                return new FightState(_target);

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("ScanState::OnStateEnter");

        }

        public void OnStateExit()
        {
            Chat.WriteLine("ScanState::OnStateExit");
        }

        private bool IsNotFightingMe(SimpleChar target)
        {
            return target.IsAttacking && target.FightingTarget.Identity != DynelManager.LocalPlayer.Identity;
        }

        private bool IsFightingAny(SimpleChar target)
        {
            if (Team.IsInTeam)
            {
                // maybe some sort of assist function??
                return (target.IsAttacking && Team.Members.Any(x => target.FightingTarget.Identity == x.Character.Identity)) ||
                    (target.IsAttacking && Team.Members.Where(x => x.Character.FightingTarget != null).Any(x => x.Character.FightingTarget.Identity == target.Identity));
            }
            else
            {
                return target.IsAttacking && target.FightingTarget.Identity == DynelManager.LocalPlayer.Identity || 
                    target.IsAttacking && DynelManager.LocalPlayer.Pets.Any(pet => target.FightingTarget.Identity == pet.Identity);
            }
        }

        public void Tick()
        {
            if (Team.IsInTeam && !DynelManager.LocalPlayer.Buffs.Contains(280470))
            {
                TeamMember leader = Team.Members
                    .Where(x => x != null)
                    .Where(x => x.Character != null)
                    .Where(x => x.IsLeader)
                    .Where(x => x.Character.DistanceFrom(DynelManager.LocalPlayer) < 30f)
                    .FirstOrDefault();

                if (leader != null)
                {
                    List<SimpleChar> mobs = DynelManager.NPCs
                        .Where(x => x.Name == "Corrupted Xan-Len" || x.Name == "Hacker'Uri" || x.Name == "The Sacrifice" ||
                                x.Name == "Blue Tower" || x.Name == "Green Tower" || x.Name == "Alien Coccoon")
                        .Where(x => leader != null)
                        .Where(x => x.DistanceFrom(leader?.Character) <= 30f)
                        .Where(x => !AutoAttackBot._namesToIgnore.Contains(x.Name))
                        .Where(x => x.IsAlive)
                        .Where(x => x.IsInLineOfSight)
                        .OrderBy(x => x.HealthPercent)
                        .ThenBy(x => x.DistanceFrom(leader?.Character))
                        .ToList();

                    SimpleChar stupidnuker = DynelManager.NPCs
                        .Where(x => x.Name == "Corrupted Xan-Kuir")
                        .Where(x => leader != null)
                        .Where(x => x.DistanceFrom(leader?.Character) <= 30f)
                        .Where(x => !AutoAttackBot._namesToIgnore.Contains(x.Name))
                        .Where(x => x.IsAlive)
                        .Where(x => x.IsInLineOfSight)
                        .FirstOrDefault();

                    List<SimpleChar> all = DynelManager.NPCs
                        .Where(x => x.Name != "Corrupted Xan-Len" || x.Name != "Hacker'Uri" || x.Name != "The Sacrifice" ||
                                x.Name != "Blue Tower" || x.Name != "Green Tower" || x.Name != "Alien Coccoon")
                        .Where(x => leader != null)
                        .Where(x => x.DistanceFrom(leader?.Character) <= 30f)
                        .Where(x => !AutoAttackBot._namesToIgnore.Contains(x.Name))
                        .Where(x => x.MaxHealth <= 999999)
                        .Where(x => x.IsAlive)
                        .Where(x => x.IsInLineOfSight)
                        .OrderBy(x => x.HealthPercent)
                        .ThenBy(x => x.DistanceFrom(leader?.Character))
                        .ToList();

                    List<SimpleChar> boss = DynelManager.NPCs
                        .Where(x => leader != null)
                        .Where(x => x.DistanceFrom(leader?.Character) <= 30f)
                        .Where(x => x.MaxHealth >= 1000000)
                        .Where(x => !AutoAttackBot._namesToIgnore.Contains(x.Name))
                        .Where(x => x.IsAlive)
                        .Where(x => x.IsInLineOfSight)
                        .ToList();


                    if (mobs.Count >= 1 && (IsFightingAny(mobs.FirstOrDefault()) || !mobs.FirstOrDefault().IsAttacking))
                        _target = mobs.FirstOrDefault();
                    else if (stupidnuker != null && mobs.Count == 0 && (IsFightingAny(stupidnuker) || !stupidnuker.IsAttacking))
                        _target = stupidnuker;
                    else if (all.Count >= 1 && stupidnuker == null && mobs.Count == 0 && (IsFightingAny(all.FirstOrDefault()) || !all.FirstOrDefault().IsAttacking))
                        _target = all.FirstOrDefault();
                    else if (boss.Count >= 1 && all.Count == 0 && mobs.Count == 0 && (IsFightingAny(boss.FirstOrDefault()) || !boss.FirstOrDefault().IsAttacking))
                        _target = boss.FirstOrDefault();
                }
                else
                {
                    List<SimpleChar> mobs = DynelManager.NPCs
                        .Where(x => x.Name == "Corrupted Xan-Len" || x.Name == "Hacker'Uri" || x.Name == "The Sacrifice" ||
                                x.Name == "Blue Tower" || x.Name == "Green Tower" || x.Name == "Alien Coccoon")
                        .Where(x => x.DistanceFrom(DynelManager.LocalPlayer) <= 30f)
                        .Where(x => !AutoAttackBot._namesToIgnore.Contains(x.Name))
                        .Where(x => x.IsAlive)
                        .Where(x => x.IsInLineOfSight)
                        .OrderBy(x => x.HealthPercent)
                        .ThenBy(x => x.DistanceFrom(DynelManager.LocalPlayer))
                        .ToList();

                    SimpleChar stupidnuker = DynelManager.NPCs
                        .Where(x => x.Name == "Corrupted Xan-Kuir")
                        .Where(x => x.DistanceFrom(DynelManager.LocalPlayer) <= 30f)
                        .Where(x => !AutoAttackBot._namesToIgnore.Contains(x.Name))
                        .Where(x => x.IsAlive)
                        .Where(x => x.IsInLineOfSight)
                        .FirstOrDefault();

                    List<SimpleChar> all = DynelManager.NPCs
                        .Where(x => x.Name != "Corrupted Xan-Len" || x.Name != "Hacker'Uri" || x.Name != "The Sacrifice" ||
                                x.Name != "Blue Tower" || x.Name != "Green Tower" || x.Name != "Alien Coccoon")
                        .Where(x => x.DistanceFrom(DynelManager.LocalPlayer) <= 30f)
                        .Where(x => !AutoAttackBot._namesToIgnore.Contains(x.Name))
                        .Where(x => x.MaxHealth <= 999999)
                        .Where(x => x.IsAlive)
                        .Where(x => x.IsInLineOfSight)
                        .OrderBy(x => x.HealthPercent)
                        .ThenBy(x => x.DistanceFrom(DynelManager.LocalPlayer))
                        .ToList();

                    List<SimpleChar> boss = DynelManager.NPCs
                        .Where(x => x.DistanceFrom(DynelManager.LocalPlayer) <= 30f)
                        .Where(x => x.MaxHealth >= 1000000)
                        .Where(x => !AutoAttackBot._namesToIgnore.Contains(x.Name))
                        .Where(x => x.IsAlive)
                        .Where(x => x.IsInLineOfSight)
                        .ToList();


                    if (mobs.Count >= 1 && (IsFightingAny(mobs.FirstOrDefault()) || !mobs.FirstOrDefault().IsAttacking))
                        _target = mobs.FirstOrDefault();
                    else if (stupidnuker != null && mobs.Count == 0 && (IsFightingAny(stupidnuker) || !stupidnuker.IsAttacking))
                        _target = stupidnuker;
                    else if (all.Count >= 1 && stupidnuker == null && mobs.Count == 0 && (IsFightingAny(all.FirstOrDefault()) || !all.FirstOrDefault().IsAttacking))
                        _target = all.FirstOrDefault();
                    else if (boss.Count >= 1 && all.Count == 0 && mobs.Count == 0 && (IsFightingAny(boss.FirstOrDefault()) || !boss.FirstOrDefault().IsAttacking))
                        _target = boss.FirstOrDefault();
                }

            }
            else if (!DynelManager.LocalPlayer.Buffs.Contains(280470))
            {
                List<SimpleChar> mobs = DynelManager.NPCs
                    .Where(x => x.Name == "Corrupted Xan-Len" || x.Name == "Hacker'Uri" || x.Name == "The Sacrifice" ||
                            x.Name == "Blue Tower" || x.Name == "Green Tower" || x.Name == "Alien Coccoon")
                    .Where(x => x.DistanceFrom(DynelManager.LocalPlayer) <= 30f)
                    .Where(x => !AutoAttackBot._namesToIgnore.Contains(x.Name))
                    .Where(x => x.IsAlive)
                    .Where(x => x.IsInLineOfSight)
                    .OrderBy(x => x.HealthPercent)
                    .ThenBy(x => x.DistanceFrom(DynelManager.LocalPlayer))
                    .ToList();

                SimpleChar stupidnuker = DynelManager.NPCs
                    .Where(x => x.Name == "Corrupted Xan-Kuir")
                    .Where(x => x.DistanceFrom(DynelManager.LocalPlayer) <= 30f)
                    .Where(x => !AutoAttackBot._namesToIgnore.Contains(x.Name))
                    .Where(x => x.IsAlive)
                    .Where(x => x.IsInLineOfSight)
                    .FirstOrDefault();

                List<SimpleChar> all = DynelManager.NPCs
                    .Where(x => x.Name != "Corrupted Xan-Len" || x.Name != "Hacker'Uri" || x.Name != "The Sacrifice" ||
                            x.Name != "Blue Tower" || x.Name != "Green Tower" || x.Name != "Alien Coccoon")
                    .Where(x => x.DistanceFrom(DynelManager.LocalPlayer) <= 30f)
                    .Where(x => !AutoAttackBot._namesToIgnore.Contains(x.Name))
                    .Where(x => x.MaxHealth <= 999999)
                    .Where(x => x.IsAlive)
                    .Where(x => x.IsInLineOfSight)
                    .OrderBy(x => x.HealthPercent)
                    .ThenBy(x => x.DistanceFrom(DynelManager.LocalPlayer))
                    .ToList();

                List<SimpleChar> boss = DynelManager.NPCs
                    .Where(x => x.DistanceFrom(DynelManager.LocalPlayer) <= 30f)
                    .Where(x => x.MaxHealth >= 1000000)
                    .Where(x => !AutoAttackBot._namesToIgnore.Contains(x.Name))
                    .Where(x => x.IsAlive)
                    .Where(x => x.IsInLineOfSight)
                    .ToList();


                if (mobs.Count >= 1 && (IsFightingAny(mobs.FirstOrDefault()) || !mobs.FirstOrDefault().IsAttacking))
                    _target = mobs.FirstOrDefault();
                else if (stupidnuker != null && mobs.Count == 0 && (IsFightingAny(stupidnuker) || !stupidnuker.IsAttacking))
                    _target = stupidnuker;
                else if (all.Count >= 1 && stupidnuker == null && mobs.Count == 0 && (IsFightingAny(all.FirstOrDefault()) || !all.FirstOrDefault().IsAttacking))
                    _target = all.FirstOrDefault();
                else if (boss.Count >= 1 && all.Count == 0 && mobs.Count == 0 && (IsFightingAny(boss.FirstOrDefault()) || !boss.FirstOrDefault().IsAttacking))
                    _target = boss.FirstOrDefault();
            }
        }
    }
}
