﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Core.Movement;
using AOSharp.Common.GameData;
using System.IO;
using AOSharp.Core.GameData;
using AOSharp.Core.UI.Options;
using AOSharp.Pathfinding;
using System.Data;
using AOSharp.Core.IPC;
using KHBuddy.IPCMessages;
using AOSharp.Core.Inventory;
using System.Collections.Concurrent;

namespace KHBuddy
{
    public class KHBuddy : AOPluginEntry
    {
        public static StateMachine _stateMachine;
        public static NavMeshMovementController NavMeshMovementController { get; private set; }
        public static IPCChannel IPCChannel { get; private set; }

        private Settings settings = new Settings("KHBuddy");

        public static List<string> _namesToIgnore;

        public static bool Auto = false;
        public static bool Defend = false;
        public static bool North = false;
        public static bool South = false;
        public static bool Beach = false;
        public static Identity Leader = Identity.None;
        public static bool IsLeader = false;

        public override void Run(string pluginDir)
        {
            Chat.WriteLine("KHBuddy Loaded!");

            try
            {
                _stateMachine = new StateMachine(new IdleState());

                Chat.RegisterCommand("khbuddy", KHBuddyCommand);

                IPCChannel = new IPCChannel(12);
                IPCChannel.RegisterCallback((int)IPCOpcode.StartDefend, OnStartMessageDefend);
                IPCChannel.RegisterCallback((int)IPCOpcode.StopDefend, OnStopMessageDefend);
                IPCChannel.RegisterCallback((int)IPCOpcode.StartAuto, OnStartMessageAuto);
                IPCChannel.RegisterCallback((int)IPCOpcode.StopAuto, OnStopMessageAuto);

                IPCChannel.RegisterCallback((int)IPCOpcode.StartBeach, OnStartMessageBeach);
                IPCChannel.RegisterCallback((int)IPCOpcode.StopBeach, OnStopMessageBeach);
                IPCChannel.RegisterCallback((int)IPCOpcode.StartNorth, OnStartMessageNorth);
                IPCChannel.RegisterCallback((int)IPCOpcode.StopNorth, OnStopMessageNorth);
                IPCChannel.RegisterCallback((int)IPCOpcode.StartSouth, OnStartMessageSouth);
                IPCChannel.RegisterCallback((int)IPCOpcode.StopSouth, OnStopMessageSouth);

                Game.OnUpdate += OnUpdate;
            }
            catch(Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }

        private void StartAuto()
        {
            Chat.WriteLine("OnStartMessage");

            Auto = true;

            if(!IsLeader && !(_stateMachine.CurrentState is IdleState))
                _stateMachine.SetState(new IdleState());
        }

        private void StopAuto()
        {
            Chat.WriteLine("OnStopMessage");

            Auto = false;

            _stateMachine.SetState(new IdleState());
        }

        private void StartDefend()
        {
            Chat.WriteLine("OnStartMessage");

            Defend = true;

            if (!IsLeader && !(_stateMachine.CurrentState is IdleState))
                _stateMachine.SetState(new IdleState());
        }

        private void StopDefend()
        {
            Defend = false;

            Chat.WriteLine("OnStopMessage");

            _stateMachine.SetState(new IdleState());
        }

        private void StartBeach()
        {
            Chat.WriteLine("OnStartMessage");

            Beach = true;

            if (!IsLeader && !(_stateMachine.CurrentState is PullState) && DynelManager.LocalPlayer.Profession == Profession.NanoTechnician)
                _stateMachine.SetState(new NukeState());

            if (!IsLeader && !(_stateMachine.CurrentState is PullState) && DynelManager.LocalPlayer.Profession != Profession.NanoTechnician)
                _stateMachine.SetState(new PullState());

        }

        private void StopBeach()
        {
            Beach = false;

            Chat.WriteLine("OnStopMessage");

            _stateMachine.SetState(new IdleState());
        }

        private void StartNorth()
        {
            Chat.WriteLine("OnStartMessage");

            North = true;

            if (!IsLeader && !(_stateMachine.CurrentState is IdleState))
                _stateMachine.SetState(new IdleState());
        }

        private void StopNorth()
        {
            North = false;

            Chat.WriteLine("OnStopMessage");

            _stateMachine.SetState(new IdleState());
        }

        private void StartSouth()
        {
            Chat.WriteLine("OnStartMessage");

            South = true;

            if (!IsLeader && !(_stateMachine.CurrentState is IdleState) && DynelManager.LocalPlayer.Profession != Profession.NanoTechnician)
                _stateMachine.SetState(new IdleState());
        }

        private void StopSouth()
        {
            South = false;

            Chat.WriteLine("OnStopMessage");

            _stateMachine.SetState(new IdleState());
        }


        private void OnStartMessageBeach(int sender, IPCMessage msg)
        {
            StartMessageBeach startMsgBeach = (StartMessageBeach)msg;

            Leader = new Identity(IdentityType.SimpleChar, sender);

            StartBeach();
        }

        private void OnStopMessageBeach(int sender, IPCMessage msg)
        {
            StopMessageBeach stopMsgBeach = (StopMessageBeach)msg;

            StopBeach();
        }

        private void OnStartMessageNorth(int sender, IPCMessage msg)
        {
            StartMessageNorth startMsgNorth = (StartMessageNorth)msg;

            Leader = new Identity(IdentityType.SimpleChar, sender);

            StartNorth();
        }

        private void OnStopMessageNorth(int sender, IPCMessage msg)
        {
            StopMessageNorth stopMsgNorth = (StopMessageNorth)msg;

            StopNorth();
        }

        private void OnStartMessageSouth(int sender, IPCMessage msg)
        {
            StartMessageSouth startMsgSouth = (StartMessageSouth)msg;

            Leader = new Identity(IdentityType.SimpleChar, sender);

            StartSouth();
        }

        private void OnStopMessageSouth(int sender, IPCMessage msg)
        {
            StopMessageSouth stopMsgSouth = (StopMessageSouth)msg;

            StopSouth();
        }

        private void OnStartMessageAuto(int sender, IPCMessage msg)
        {
            StartMessageAuto startMsgAuto = (StartMessageAuto)msg;

            Leader = new Identity(IdentityType.SimpleChar, sender);

            StartAuto();
        }

        private void OnStopMessageAuto(int sender, IPCMessage msg)
        {
            StopMessageAuto stopMsgAuto = (StopMessageAuto)msg;

            StopAuto();
        }



        private void OnStartMessageDefend(int sender, IPCMessage msg)
        {
            StartMessageDefend startMsgDefend = (StartMessageDefend)msg;

            Leader = new Identity(IdentityType.SimpleChar, sender);

            StartDefend();
        }

        private void OnStopMessageDefend(int sender, IPCMessage msg)
        {
            StopMessageDefend stopMsgDefend = (StopMessageDefend)msg;

            StopDefend();
        }



        private void OnUpdate(object s, float deltaTime)
        {
            if (Game.IsZoning)
                return;

            _stateMachine.Tick();

            if (!IsLeader)
                return;
        }

        private void PrintKHBuddyCommandUsage(ChatWindow chatWindow)
        {
            string help = "For toggle;\n" +
                            "/kh side." +
                            "\n" +
                            "\n" +
                            "Side Syntax;\n" +
                            "Beach, North and South.";

            chatWindow.WriteLine(help, ChatColor.LightBlue);
        }

        private void KHBuddyCommand(string command, string[] param, ChatWindow chatWindow)
        {
            try
            {
                if (param.Length < 1)
                {
                    PrintKHBuddyCommandUsage(chatWindow);
                    return;
                }

                if (param[0].ToLower() == "help")
                {
                    PrintKHBuddyCommandUsage(chatWindow);
                    return;
                }

                if (param[0].ToLower() == "beach")
                {
                    if (South)
                    {
                        Chat.WriteLine("You have to disable South, /kh south.");
                        return;
                    }

                    if (North)
                    {
                        Chat.WriteLine("You have to disable North, /kh North.");
                        return;
                    }

                    if (Beach)
                    {
                        StopBeach();
                        IPCChannel.Broadcast(new StopMessageBeach());
                    }
                    else
                    {
                        StartBeach();
                        IPCChannel.Broadcast(new StartMessageBeach());
                    }
                }

                if (param[0].ToLower() == "south")
                {
                    if (Beach)
                    {
                        Chat.WriteLine("You have to disable South, /kh south.");
                        return;
                    }

                    if (North)
                    {
                        Chat.WriteLine("You have to disable North, /kh North.");
                        return;
                    }

                    if (South)
                    {
                        StopSouth();
                        IPCChannel.Broadcast(new StopMessageSouth());
                    }
                    else
                    {
                        StartSouth();
                        IPCChannel.Broadcast(new StartMessageSouth());
                    }
                }

                if (param[0].ToLower() == "north")
                {
                    if (South)
                    {
                        Chat.WriteLine("You have to disable South, /kh south.");
                        return;
                    }

                    if (Beach)
                    {
                        Chat.WriteLine("You have to disable North, /kh North.");
                        return;
                    }

                    if (North)
                    {
                        StopNorth();
                        IPCChannel.Broadcast(new StopMessageNorth());
                    }
                    else
                    {
                        StartNorth();
                        IPCChannel.Broadcast(new StartMessageNorth());
                    }
                }
            } 
            catch (Exception e)
            {
                Chat.WriteLine(e.Message);
            }
        }
    }
}
