﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace KHBuddy
{
    public class NukeState : IState
    {
        public const double FightTimeout = 1080f;
        public const double RefreshMongoTime = 8f;
        public const double RefreshAbsorbTime = 11f;
        private SimpleChar _target;
        private double _fightStartTime;
        private double _refreshMongoTimer;
        private double _refreshAbsorbTimer;
        public static float _tetherDistance;
        public SimpleChar LeaderChar;
        Spell aoenukes = null;
        Spell mongobuff = null;
        Spell absorb = null;

        public static bool InternelSwitch = false;

        public IState GetNextState()
        {
            if (Time.NormalTime > _fightStartTime + FightTimeout && InternelSwitch == true &&
                DynelManager.LocalPlayer.Profession == Profession.Enforcer)
            {
                InternelSwitch = false;
                return new PullState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("NukeState::OnStateEnter");
        }

        public void OnStateExit()
        {
            Chat.WriteLine("NukeState::OnStateExit");
        }

        private static class RelevantNanos
        {
            public static readonly int[] AOENukes = {266293, 266294, 266295, 266296, 28620, 28638,
                266297, 28637, 28594, 45922, 45906, 45884, 28635, 266298, 28593, 45925, 45940, 45900,28629,
                45917, 45937, 28599, 45894, 45943, 28633, 28631 };
        }

        public void Tick()
        {
            List<SimpleChar> hecks = DynelManager.NPCs
                .Where(x => x.Name.Contains("Heckler"))
                .Where(x => x.DistanceFrom(DynelManager.LocalPlayer) <= 60f)
                .Where(x => x.IsAlive)
                .Where(x => x.IsInLineOfSight)
                .ToList();
            

            if (hecks.Count == 0 && InternelSwitch == false)
            {
                _fightStartTime = Time.NormalTime;
                InternelSwitch = true;
            }


            if (DynelManager.LocalPlayer.Profession == Profession.NanoTechnician)
            {
                if (aoenukes == null)
                    aoenukes = Spell.List.Where(x => RelevantNanos.AOENukes.Contains(x.Identity.Instance)).FirstOrDefault();

                if (hecks.Count >= 1)
                {
                    if (!Spell.HasPendingCast && aoenukes.IsReady)
                    {
                        aoenukes.Cast(hecks.FirstOrDefault(), true);
                    }
                }

            }

            if (DynelManager.LocalPlayer.Profession == Profession.Enforcer)
            {
                if (mongobuff == null)
                    mongobuff = Spell.List.Where(x => x.Nanoline == NanoLine.MongoBuff).OrderBy(x => x.StackingOrder).FirstOrDefault();
                if (absorb == null)
                    absorb = Spell.List.Where(x => x.Nanoline == NanoLine.AbsorbACBuff).OrderBy(x => x.StackingOrder).FirstOrDefault();

                if (hecks.Count >= 1)
                {
                    if (!Spell.HasPendingCast && mongobuff.IsReady && Time.NormalTime > _refreshMongoTimer + RefreshMongoTime)
                    {
                        mongobuff.Cast();
                        _refreshMongoTimer = Time.NormalTime;
                    }
                    if (!Spell.HasPendingCast && absorb.IsReady && Time.NormalTime > _refreshAbsorbTimer + RefreshAbsorbTime)
                    {
                        absorb.Cast();
                        _refreshAbsorbTimer = Time.NormalTime;
                    }
                }
            }
        }
    }
}
