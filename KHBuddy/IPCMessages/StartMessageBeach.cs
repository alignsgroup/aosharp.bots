﻿using System;
using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;

namespace KHBuddy.IPCMessages
{
    [AoContract((int)IPCOpcode.StartBeach)]
    public class StartMessageBeach : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.StartBeach;
    }
}
