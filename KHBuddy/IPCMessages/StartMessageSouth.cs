﻿using System;
using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;

namespace KHBuddy.IPCMessages
{
    [AoContract((int)IPCOpcode.StartSouth)]
    public class StartMessageSouth : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.StartSouth;
    }
}
