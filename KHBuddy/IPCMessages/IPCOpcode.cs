﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KHBuddy.IPCMessages
{
    public enum IPCOpcode
    {
        StartDefend = 1001,
        StopDefend = 1002,
        StartAuto = 1003,
        StopAuto = 1004,
        StartBeach = 1005,
        StopBeach = 1006,
        StartNorth = 1007,
        StopNorth = 1008,
        StartSouth = 1009,
        StopSouth = 1010,
        SetPos = 1011
    }
}
