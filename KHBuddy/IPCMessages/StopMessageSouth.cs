﻿using System;
using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;

namespace KHBuddy.IPCMessages
{
    [AoContract((int)IPCOpcode.StopSouth)]
    public class StopMessageSouth : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.StopSouth;
    }
}
