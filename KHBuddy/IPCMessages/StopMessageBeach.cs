﻿using System;
using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;

namespace KHBuddy.IPCMessages
{
    [AoContract((int)IPCOpcode.StopBeach)]
    public class StopMessageBeach : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.StopBeach;
    }
}
