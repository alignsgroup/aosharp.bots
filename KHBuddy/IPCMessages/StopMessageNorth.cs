﻿using System;
using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;

namespace KHBuddy.IPCMessages
{
    [AoContract((int)IPCOpcode.StopNorth)]
    public class StopMessageNorth : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.StopNorth;
    }
}
