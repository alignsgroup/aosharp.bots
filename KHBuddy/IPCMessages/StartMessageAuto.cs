﻿using System;
using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;

namespace KHBuddy.IPCMessages
{
    [AoContract((int)IPCOpcode.StartAuto)]
    public class StartMessageAuto : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.StartAuto;
    }
}
