﻿using System;
using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;

namespace KHBuddy.IPCMessages
{
    [AoContract((int)IPCOpcode.StartDefend)]
    public class StartMessageDefend : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.StartDefend;
    }
}
