﻿using System;
using AOSharp.Core.IPC;
using SmokeLounge.AOtomation.Messaging.Serialization.MappingAttributes;

namespace KHBuddy.IPCMessages
{
    [AoContract((int)IPCOpcode.StartNorth)]
    public class StartMessageNorth : IPCMessage
    {
        public override short Opcode => (short)IPCOpcode.StartNorth;
    }
}
