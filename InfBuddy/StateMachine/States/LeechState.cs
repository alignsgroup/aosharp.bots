﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Movement;
using AOSharp.Core.UI;

namespace InfBuddy
{
    public class LeechState : IState
    {
        private bool _missionsLoaded = false;

        public IState GetNextState()
        {
            if (Playfield.ModelIdentity.Instance == Constants.InfernoId)
                return new ReformState();

            if (_missionsLoaded && !Mission.List.Exists(x => x.DisplayName.Contains(InfBuddy.ActiveGlobalSettings.GetMissionName())) && !InfBuddy.Config.DoubleReward)
                return new ExitMissionState();

            if ((_missionsLoaded && !Mission.List.Exists(x => x.DisplayName.Contains("The Purification Ritual - Me...")) && !Mission.List.Exists(x => x.DisplayName.Contains("The Purification Ritual - Ha...")) && InfBuddy.Config.DoubleReward && !Team.IsLeader))
            {
                if (InfBuddy.triggerswitch == true)
                    InfBuddy.triggerswitch = false;
                else
                    InfBuddy.triggerswitch = true;

                return new ExitMissionState();
            }

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("LeechState::OnStateEnter");

            DynelManager.LocalPlayer.Position = Constants.LeechSpot;
            MovementController.Instance.SetMovement(MovementAction.Update);
        }

        public void OnStateExit()
        {
            Chat.WriteLine("LeechState::OnStateExit");
        }

        public void Tick()
        {
            //Fix for check happening before missions are loaded on zone
            if (Mission.List.Exists(x => x.DisplayName.Contains(InfBuddy.ActiveGlobalSettings.GetMissionName())))
                _missionsLoaded = true;
            if (Mission.List.Exists(x => x.DisplayName.Contains("The Purification Ritual - Me...")) && InfBuddy.Config.DoubleReward)
                _missionsLoaded = true;
        }
    }
}
