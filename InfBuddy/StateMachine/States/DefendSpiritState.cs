﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.Inventory;
using AOSharp.Core.UI;
using System;
using System.Linq;

namespace InfBuddy
{
    public class DefendSpiritState : PositionHolder, IState
    {
        private SimpleChar _target;
        public const double MobStuckTimeout = 1500f;
        private double _mobStuckStartTime;
        private bool switchy = false;

        public DefendSpiritState() : base(Constants.DefendPos, 3f, 1)
        {

        }

        public IState GetNextState()
        {
            if (Playfield.ModelIdentity.Instance != Constants.InfernoId && Playfield.ModelIdentity.Instance != Constants.NewInfMissionId)
                return new IdleState();

            if (DynelManager.Exists(Constants.PlayerKillers))
            {
                foreach (Mission mission in Mission.List)
                {
                    if (mission.DisplayName.Contains("The Purification"))
                        mission.Delete();
                }

                InfBuddy.triggerswitch = false;

                Chat.WriteLine("Mission has failed, reforming");

                return new ExitMissionState();
            }

            if (!Mission.List.Exists(x => x.DisplayName.Contains(InfBuddy.ActiveGlobalSettings.GetMissionName())) && !InfBuddy.Config.DoubleReward)
            {
                Chat.WriteLine($"Couldn't find mission {InfBuddy.ActiveGlobalSettings.GetMissionName()}");
                return new ExitMissionState();
            }

            if (!Mission.List.Exists(x => x.DisplayName.Contains("The Purification Ritual - Me...")) && !Mission.List.Exists(x => x.DisplayName.Contains("The Purification Ritual - Ha...")) && InfBuddy.Config.DoubleReward && !Team.IsLeader)
            {
                if (InfBuddy.triggerswitch == true)
                    InfBuddy.triggerswitch = false;
                else
                    InfBuddy.triggerswitch = true;

                return new ExitMissionState();
            }

            if (Time.NormalTime > _mobStuckStartTime + MobStuckTimeout)
            {
                return new ExitMissionState();
            }

            if (_target != null)
                return new FightState(_target);

            return null;
        }

        public void OnStateEnter()
        {
            Chat.WriteLine("DefendSpiritState::OnStateEnter");

            _mobStuckStartTime = Time.NormalTime;

            HoldPosition();
        }

        public void OnStateExit()
        {
            Chat.WriteLine("DefendSpiritState::OnStateExit");
        }

        public void Tick()
        {
            foreach (SimpleChar npc in DynelManager.NPCs)
            {
                if (!npc.IsAlive)
                    continue;

                if (npc.Name == Constants.SpiritNPCName)
                    continue;

                if (npc.Name == Constants.QuestStarterName)
                    continue;

                //We're plenty capable of pathing to mobs but we get way more consistent results if we just stand still and attack anything that walks up.
                //if (!npc.IsInAttackRange())
                //    continue;

                if (npc.Position.DistanceFrom(Constants.DefendPos) < 33f)
                {
                    switchy = false;
                    _target = npc;
                }

                //if (!npc.IsAlive)
                //    continue;

                //if (npc.Name == Constants.SpiritNPCName)
                //    continue;

                //if (npc.Name == Constants.QuestStarterName)
                //    continue;

                //_target = npc;
            }

            if (InfBuddy.NavMeshMovementController.IsNavigating && switchy == false &&
                DynelManager.NPCs.Where(x => x.Position.DistanceFrom(Constants.DefendPos) < 33f && x.Name != Constants.SpiritNPCName).Count() == 0)
            {
                switchy = true;
                InfBuddy.NavMeshMovementController.Halt();
            }

            HoldPosition();
        }
    }
}
